<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed title
 * @property mixed description
 * @property mixed created_at
 * @property mixed user
 * @property mixed user_id
 */
class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'description'=> $this->description,
            'created_at'=> $this->created_at->ToDateString(),
            'user'=> new UserResource($this->user)
        ];
    }
}
