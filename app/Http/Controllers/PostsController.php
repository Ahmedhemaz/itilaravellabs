<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\StorePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\User;
use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    public function renderLandingPage(){

        return view('welcome');
    }

    public function index(){

        $posts = Post::paginate(10);
        return view('posts/index',[
            'posts' => $posts
        ]);
    }

    public function create(){

        $users = User::all();
        return view('posts.create',[
            'users'=>$users
        ]);
    }

    public function store(StorePostRequest $request){

        Post::create($request->only([
            'title','description','user_id'
        ]));

        return redirect()->route('posts.index');
    }

    public function show($post){
        if(is_string($post)){
            $post = Post::where('slug',$post)->first();
        }else{
            $post = Post::where('id',$post)->first();
        }

        return view('posts.show',[
            'post'=>$post,
        ]);
    }

    public function edit(Post $post){

        $users = User::all();
        return view('posts.edit',[
            'post'=>$post,
            'users'=>$users
        ]);
    }

    public function update(UpdatePostRequest $request,Post $post){

        $post->update($request->all());


        return redirect()->route('posts.index');
    }

    public function destroy(Post $post){
        $post->delete();
        return redirect()->route('posts.index');
    }



}
