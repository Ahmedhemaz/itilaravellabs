<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Post\StorePostRequest;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class PostsController extends Controller
{
    public function index(){

        return PostResource::collection(Post::paginate(1));
    }

    public function show($post){
        $post = Post::findOrFail($post);
        return new PostResource($post);
    }

    public function store(StorePostRequest $request){

        Post::create($request->all());
        return response()->json([
            'message'=> 'post created successfully',

        ],201);
    }
}
