<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:3',
            'description'=>'required|min:10',
            'user_id'=>'exists:posts,user_id'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'title is required',
            'title.min' => 'title must be more than 3 characters',
            'description.required' => 'description is required',
            'description.min' => 'description must be more than 10 characters'
        ];
    }
}
