<?php

namespace App;

//use Carbon\Carbon;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Sluggable;

    protected $fillable = [
        'title','description','user_id'
    ];


    public function user(){

        return $this->belongsTo(User::class);
    }

    public function humanReadableDate(){
        return $this->created_at->format('l jS \\of F Y h:i:s A');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
