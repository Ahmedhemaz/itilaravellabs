@extends('layouts.app')


@section('content')
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Title</th>
        <th scope="col">Slug Title</th>
        <th scope="col">Creator</th>
        <th scope="col">Date</th>
        <th scope="col">Methods</th>
    </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
    <tr>
        <th scope="row">{{$post->id}}</th>
        <td>{{$post->title}}</td>
        <td>{{isset($post->slug)? $post->slug : "NONE"}}</td>
        <td>{{ isset($post->user) ? $post->user->name : "Not Found"}}</td>
        <td>{{ isset($post->created_at) ? $post->created_at->toDateString() : "None"}}</td>
        <td>
            <a href="{{route('posts.show',isset($post->slug)? $post->slug : $post->id)}}"><i class="fas fa-eye"></i></a> |
            <a href="{{route('posts.edit', isset($post->slug)? $post->slug : $post->id)}}" ><i class="fas fa-pen"></i></a>  |
            <a href="#" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash-alt"></i></a>
        </td>
        <div class="modal fade" id="myModal" role="dialog">
            <form action="{{route('posts.destroy', $post->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-body">
                            <p>Are You Sure You Want To Delete This Post</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="yes" class="btn btn-default">Yes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </tr>
    @endforeach
    </tbody>
</table>
<a class="btn btn-primary btn-lg col-4" href="{{route('posts.create')}}" style="margin-bottom: 20px; margin-left: 30%;">Create Post</a>

{{$posts->links()}}

@endsection
