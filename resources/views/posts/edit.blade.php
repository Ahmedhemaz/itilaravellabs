@extends('layouts.app')


@section('content')

    <form action="{{route('posts.update' , $post->id)}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label" >Title</label>
            <input type="text" name="title" class="form-control col-3" value="{{$post->title}}" placeholder="Enter Title">
        </div>
        @if ($errors->get('title'))
            <div class="alert alert-danger col-sm-6" style="margin-left: 20px" >
                <ul>
                    @foreach ($errors->get('title') as $title)
                        <li>{{ $title }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label" >Description</label>
            <textarea class="form-control col-4"  name="description" rows="3">{{$post->description}}</textarea>
        </div>
        @if ($errors->get('description'))
            <div class="alert alert-danger col-sm-6" style="margin-left: 20px" >
                <ul>
                    @foreach ($errors->get('description') as $description)
                        <li>{{ $description }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group row">
            <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">Example select</label>
            <select class="form-control col-4" name="user_id">
                @foreach($users as $user)
                    <option value="{{$user->id}}" >{{$user->name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>


@endsection
