@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>{{$post->title}}</h1>
            </div>
        </div>
        <h5>{{$post->description}}</h5>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Created At</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ isset($post->user) ? $post->user->name : "Not Found"}}</td>
                <td>{{ isset($post->user) ? $post->user->email : "Not Found"}}</td>
                <td>{{ isset($post->created_at) ? $post->humanReadableDate() : "None"}}</td>
            </tr>
        </tbody>
    </table>
@endsection
